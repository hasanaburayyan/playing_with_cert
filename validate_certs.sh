declare CA_name="certs/rootca.pem"
declare keyname="certs/root.key"
declare int_key="certs/intermediate1.key"
declare int_csr="certs/intermediate1.csr"
declare int_crt="certs/intermediate1.crt"
declare sub_int_key="certs/intermediate2.key"
declare sub_int_csr="certs/intermediate2.csr"
declare sub_int_crt="certs/intermediate2.crt"
declare user_crt="certs_users/enduser.crt"
declare user_key="certs_users/enduser.key"
declare user_chain="certs_users/enduser.chain"


openssl verify -CAfile $user_chain $user_crt
