declare bits=4096
declare CA_name="certs/rootca.pem"
declare keyname="certs/root.key"
declare int_key="certs/intermediate1.key"
declare int_csr="certs/intermediate1.csr"
declare int_crt="certs/intermediate1.crt"
declare sub_int_key="certs/intermediate2.key"
declare sub_int_csr="certs/intermediate2.csr"
declare sub_int_crt="certs/intermediate2.crt"
declare int_bits=8192
declare days=3065
declare country="US"
declare state="Ohio"
declare city="Columbus"
declare common="www.hasan.com"

rm -rf certs/config/*
rm -rf certs/*
rm -rf certs_users/*
mkdir certs/config

# Generate Root Key
openssl genrsa \
-out \
$keyname \
$bits

# Generate Root CA
openssl req \
-x509 \
-new \
-nodes \
-key $keyname \
-sha256 \
-days $days \
-subj "/C=${country}/ST=${state}/L=${city}/CN=${common}" \
-out $CA_name

# Create index files
touch certs/config/certindex
echo 1000 > certs/config/certserial
echo 1000 > certs/config/crlnumber

# Generate The CA Config file This is a pretty basice
# file that will default to 5 year CA
cat << EOF > certs/ca.conf
[ ca ]
default_ca = myca

[ crl_ext ]
issuerAltName=issuer:copy
authorityKeyIdentifier=keyid:always

[ myca ]
dir = certs
new_certs_dir = \$dir
unique_subject = no
certificate = \$dir/rootca.pem
database = \$dir/config/certindex
private_key = \$dir/root.key
serial = \$dir/config/certserial
default_days = 1860
default_md = sha256
policy = myca_policy
x509_extensions = myca_extensions
crlnumber = \$dir/config/crlnumber
default_crl_days = 1860

[ myca_policy ]
commonName = supplied
stateOrProvinceName = supplied
countryName = optional
emailAddress = optional
organizationName = optional
organizationalUnitName = optional

[ myca_extensions ]


[ v3_ca ]
subjectAltName = @alt_names

[alt_names]
DNS.0 = 127.0.0.1

[crl_section]

[ocsp_section]
EOF

# Generate Int Key
openssl genrsa \
-out $int_key $int_bits

# Generate Int CSR
openssl req \
-sha256 \
-new \
-key $int_key \
-subj "/C=${country}/ST=${state}/L=${city}/CN=www.hasan2.com" \
-out $int_csr

# Generate Int cert
openssl ca \
-batch \
-config certs/ca.conf \
-notext \
-in $int_csr \
-out $int_crt

# Create index files
touch certs/config/certindex_intermediate1
echo 1000 > certs/config/certserial_intermediate1
echo 1000 > certs/config/crlnumber_intermediate1

# Generate The Int CA Config file This is a pretty basice
# file that will default to 2 year CA
cat << EOF > certs/int_ca.conf
[ ca ]
default_ca = myca

[ crl_ext ]
issuerAltName=issuer:copy
authorityKeyIdentifier=keyid:always

[ myca ]
dir = certs
new_certs_dir = \$dir
unique_subject = no
certificate = \$dir/intermediate1.crt
database = \$dir/config/certindex_intermediate1
private_key = \$dir/intermediate1.key
serial = \$dir/config/certserial_intermediate1
default_days = 730
default_md = sha256
policy = myca_policy
x509_extensions = myca_extensions
crlnumber = \$dir/config/crlnumber_intermediate1
default_crl_days = 730

[ myca_policy ]
commonName = supplied
stateOrProvinceName = supplied
countryName = optional
emailAddress = optional
organizationName = optional
organizationalUnitName = optional

[ myca_extensions ]


[ v3_ca ]
subjectAltName = @alt_names

[alt_names]
DNS.0 = 127.0.0.1

[crl_section]

[ocsp_section]
EOF

# Generate Int Key
openssl genrsa \
-out $sub_int_key $int_bits

# Generate Int CSR
openssl req \
-sha256 \
-new \
-key $sub_int_key \
-subj "/C=${country}/ST=${state}/L=${city}/CN=www.hasan3.com" \
-out $sub_int_csr

# Generate Int cert
openssl ca \
-batch \
-config certs/int_ca.conf \
-notext \
-in $sub_int_csr \
-out $sub_int_crt

# Create index files
touch certs/config/certindex_intermediate2
echo 1000 > certs/config/certserial_intermediate2
echo 1000 > certs/config/crlnumber_intermediate2

# Generate The Sub INT Config file This is a pretty basice
# file that will default to 2 year CA
cat << EOF > certs/sub_int_ca.conf
[ ca ]
default_ca = myca

[ crl_ext ]
issuerAltName=issuer:copy
authorityKeyIdentifier=keyid:always

[ myca ]
dir = certs
new_certs_dir = \$dir
unique_subject = no
certificate = \$dir/intermediate2.crt
database = \$dir/config/certindex_intermediate2
private_key = \$dir/intermediate2.key
serial = \$dir/config/certserial_intermediate2
default_days = 365
default_md = sha256
policy = myca_policy
x509_extensions = myca_extensions
crlnumber = \$dir/config/crlnumber_intermediate2
default_crl_days = 365

[ myca_policy ]
commonName = supplied
stateOrProvinceName = supplied
countryName = optional
emailAddress = optional
organizationName = optional
organizationalUnitName = optional

[ myca_extensions ]


[ v3_ca ]
subjectAltName = @alt_names

[alt_names]
DNS.0 = 127.0.0.1

[crl_section]

[ocsp_section]
EOF


# Create Certificate Chain
cat $CA_name $int_crt $sub_int_crt > certs_users/enduser.chain


# Generete end users csr
openssl genrsa -out certs_users/enduser.key 4096
openssl req -new -sha256 -key certs_users/enduser.key -subj "/C=${country}/ST=${state}/L=${city}/CN=www.user.com" -out certs_users/enduser.csr
openssl ca -batch -config certs/sub_int_ca.conf -notext -in certs_users/enduser.csr -out certs_users/enduser.crt